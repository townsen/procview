# List of Changes

In reverse chronological order:

## 0.9.0
Thursday March 13th, 2014

Refactored the code so that enhancing it was easier:

* Moved command line parsing into Options class
* Created a Runner class for running it, taking logic out of the binary for easier testing
* Changed the class/module name ProcView to Procview
* Made Procview a module and the old ProcView class became Procview::Analyzer
* Added a CHANGELOG

Improved the README and included it and CHANGELOG in the `rdoc` options for the gem.

## 0.8.9
Wednesday March 12th, 2014

Additional calls, thanks to Colin Liebermann for the diagnostics:

* Added call support for `sendmsg`
* Improved handle processing for `close`, `accept4` by ignoring 'Bad File Descriptor'
* Improved sockaddr processing to detect when the structure is NULL or empty
