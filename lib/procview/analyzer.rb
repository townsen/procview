# Analyze the tracefile entries
#
require 'set'
require 'procview/straceargs'
require 'procview/stats'

module Procview

  class Analyzer

    PSTATS  = Struct.new(:cwd, :hmap, :emap, :resume)

    attr_reader :calls, :dregs, :iomap, :ipid, :pstats, :sigmap, :warnings, :info
    attr_writer :inithandles
    attr_accessor :pwd

    def infomsg msg
      @info.push ($. > 0 ? "#{$.}: ":'')+msg
    end

    def warning msg
      @warnings.push ($. > 0 ? "#{$.}: ":'')+msg
    end

    def initPstats parent = nil, same_wd = false, same_fds = false
      if parent
        cwd = same_wd ? parent.cwd : parent.cwd.clone
        hmap = same_fds ? parent.hmap : parent.hmap.clone
        emap = same_fds ? parent.emap : parent.emap.clone
        return PSTATS.new( cwd, hmap, emap, [] )
      else
        stats = PSTATS.new(
          @pwd,
          Hash.new do |h,k|
            warning "Unknown handle #{k} (Pid #{@pid})"
            h[k] = "h#{k}".to_sym
          end,
          Hash.new{|h,k| h[k] = Set.new},
          []
        )
        @inithandles.each {|k,v| stats.hmap[k] = v} if @inithandles
        return stats
      end
    end

    def initialize opts = { debug: nil, missing: true }
      @calls  = Hash.new {|h,k| h[k] = [0,0.0]}
      @iomap  = Hash.new{|h,k| h[k] = Procview::Stats.new}
      @pstats = Hash.new{|h,k| h[k] = initPstats }
      @ipid   = nil # the initial pid
      @pid    = nil # the current pid (same as above if just one process)
      @sigmap = Hash.new(0)
      @dregs  = 0.0 # store unaccounted for time here
      @pwd    = nil
      @inithandles = nil
      @opts   = opts
      @info   = []
      @warnings = []
    end

    def load io
      while line = io.gets
        @opts[:debug].puts line if @opts[:debug]
        begin
          warning line if digest(line) > 0
        rescue StandardError => e
          warning "#{e.message} (#{e.backtrace[0]}): #{line}"
        end
      end
    end

    def digest line

      # Complex as we cope with traces with no pids
      # and traces where the initial LSOF lines don't
      # know the pid (debug output on a command trace)
      #
      if line.slice!(/^(\d+)\s+/)
        @pid = $1.to_i
        if @ipid == -1
          @pstats[@pid] = @pstats.delete(@ipid)
          @ipid = @pid
        end
      else
        @pid = @ipid
      end

      case line
      when /^TIME\s+/
        infomsg "TRACED on #{$'}"

      when /^REQUEST\s+/
        infomsg $'

      when /^LSOF/
        fields = line.split(/\s+/)
        unless fields.size > 9
          warning "Too few fields"
          return 1
        end
        pid = fields[2].to_i
        fname = fields[9..-1].join(' ')
        case fields[4]
        when 'cwd'
          raise "PID set prior to cwd" if @ipid
          @ipid = pid
          @pstats[@ipid].cwd = fname
        when /(\d+)\w/
          handle = $1.to_i
          @pstats[@ipid].hmap[handle] = fname
        else
          warning "Invalid handle #{fields[4]}"
          return 1
        end

      when / <unfinished ...>/
        @pstats[@pid].resume.push $`

      when /<... (\w+) resumed> /
        call = $1.to_sym
        rest = $'.chomp
        prev = @pstats[@pid].resume.find_index {|f| f =~ /^#{call}/}
        unless prev
          warning "Failed to find resumable call #{call}"
          return 1
        end
        first = @pstats[@pid].resume.delete_at(prev)
        return digest (@pid.to_s+" "+first+rest)

      when /^(?<call>\w+)\((?<args>.*)\)\s+=\s+\?/x # process termination has ?
        return 0

      when /^(?<call>\w+)
        \((?<args>.*)\)\s+=\s+(?<rc>-?(\d+|\?))\s*
          (?<ret>.*)\s+
          <(?<duration>.*)>$
        /x
        return handleCall($1.to_sym, $2, $3.to_i, $4, $5.to_f)

      when /^---\s*(\w+)\s*/
        signal = $1.to_sym
        @sigmap[signal] += 1
      when /^+++.*+++$/
      else
        warning "Unknown line format"
        return 1
      end
      return 0
    end

    private

    # Accumulate time into the handle map or IO map
    # Often handles are created before the ultimate
    # destination is known, so the time is accumulated
    # in the handle map rather than the IO map
    # eg.socket() then bind()

    def accumulate hmap, fd, type, duration
      fds = hmap[fd]
      fds = @iomap[fds] unless fds.is_a? Stats
      fds.acc! type, duration
    end

    # A call that previously accumulated to a handle
    # now has an IO name, so move it to the IO map
    # Now connect can be called several times if
    # socket is non-blocking, in which case this is
    # already done

    def movetoIOmap name, fd, hmap
      stats = hmap[fd]
      return unless stats.is_a? Procview::Stats
      stats.add! @iomap[name] if @iomap.include? name
      @iomap[name] = stats
      hmap[fd] = name
    end

    public

    def handleCall call, args, rc, ret, duration
      pstat = @pstats[@pid]
      case call

      when :accept, :accept4 
        fd = StraceArgs.fd! args
        accumulate pstat.hmap, fd, :other, duration
        pstat.hmap[rc] = (StraceArgs.sockaddr!(args) || "#{call}#{rc}") if rc > 0

      when :clone
        StraceArgs.nextcomma! args
        flags = StraceArgs.flags! args
        @pstats[rc] = initPstats @pstats[@pid], flags.include?(:CLONE_FS), flags.include?(:CLONE_FILES)
        @dregs += duration

      when :stat, :lstat
        file = StraceArgs.name! args
        if rc == -1
          if @opts[:missing]
            @dregs += duration
          else
            @iomap[File.absolute_path(file, pstat.cwd)].acc! :other, duration
          end
        else
          @iomap[File.absolute_path(file, pstat.cwd)].acc! :other, duration
        end

      when :rename, :symlink
        f1 = StraceArgs.name! args
        f2 = StraceArgs.name! args
        @iomap[File.absolute_path(f1, pstat.cwd)].acc! :other, duration/2.0
        @iomap[File.absolute_path(f2, pstat.cwd)].acc! :other, duration/2.0

      when :execve, :access, :readlink, :chmod
        file = StraceArgs.name! args
        @iomap[File.absolute_path(file, pstat.cwd)].acc! :other, duration

      when :open
        file = StraceArgs.name! args
        if rc == -1
          if @opts[:missing]
            @dregs += duration
          else
            @iomap[File.absolute_path(file, pstat.cwd)].acc! :other, duration
          end
        else
          pstat.hmap[rc] = File.absolute_path(file, pstat.cwd)
          accumulate pstat.hmap, rc, :other, duration
        end

      when :mkdir, :rmdir, :getcwd, :statfs, :utimes, :unlink
        file = StraceArgs.name! args
        @iomap[File.absolute_path(file, pstat.cwd)].acc! :other, duration

      when :chdir
        file = StraceArgs.name! args
        pstat.cwd = File.absolute_path(file, pstat.cwd)
        @iomap[pstat.cwd].acc! :other, duration

      when :close
        fd = StraceArgs.fd! args
        inhmap = pstat.hmap.include? fd
        if rc == -1
          @dregs += duration
          raise "Handle map contains non-existent fd #{fd}" if inhmap
        end
        if inhmap
          accumulate pstat.hmap, fd, :other, duration
          pstat.hmap.delete fd
        end
        # Strace IO flags don't collect epoll_create so check assumption it's emap
        unless rc == -1 or inhmap or pstat.emap.include? fd
          warning "Neither handle map or emap include #{fd}"
        end

      when :fcntl
        fd = StraceArgs.fd! args
        op = StraceArgs.op! args
        if [:F_DUPFD, :F_DUPFD_CLOEXEC].include? op
          pstat.hmap[rc] = pstat.hmap[fd]
        end
        accumulate pstat.hmap, fd, :other, duration

      when :pipe, :pipe2
        fds = StraceArgs.afds! args
        pstat.hmap[fds[0]] = call
        accumulate pstat.hmap, fds[0], :other, duration/2.0
        pstat.hmap[fds[1]] = call
        accumulate pstat.hmap, fds[1], :other, duration/2.0

      when :eventfd, :eventfd2
        fd = StraceArgs.fd! args
        pstat.hmap[fd] = call
        accumulate pstat.hmap, fd, :other, duration

      when :socket
        pstat.hmap[rc] = Stats.new
        accumulate pstat.hmap, rc, :other, duration

      when :getsockopt, :setsockopt
        fd = StraceArgs.fd! args
        accumulate pstat.hmap, fd, :other, duration

      when :connect, :getpeername
        fd = StraceArgs.fd! args
        name = StraceArgs.sockaddr! args
        accumulate pstat.hmap, fd, :other, duration
        movetoIOmap name, fd, pstat.hmap

      when :bind
        fd = StraceArgs.fd! args
        name = StraceArgs.sockaddr! args
        accumulate pstat.hmap, fd, :other, duration
        movetoIOmap name, fd, pstat.hmap

      when :listen
        fd = StraceArgs.fd! args
        accumulate pstat.hmap, fd, :other, duration

      when :fsync, :fdatasync
        fd = StraceArgs.fd! args
        accumulate pstat.hmap, fd, :other, duration

      when :dup, :dup2, :dup3
        fd = StraceArgs.fd! args
        pstat.hmap[rc] = pstat.hmap[fd]
        accumulate pstat.hmap, fd, :other, duration

      when :flock, :fstat, :ioctl, :getsockname, :shutdown
        fd = StraceArgs.fd! args
        accumulate pstat.hmap, fd, :other, duration

      # Calls classed as 'wait'

      when :select
        max = StraceArgs.fd! args
        if max > 0
          rfds = StraceArgs.afds!(args).to_set
          wfds = StraceArgs.afds!(args).to_set
          efds = StraceArgs.afds!(args).to_set
          fds = rfds+wfds+efds
        else
          fds = Set.new
        end
        if fds.size > 0
          fds.each{|f| accumulate pstat.hmap, f, :wait, duration/fds.size}
        else
          @dregs += duration
        end

      when :poll, :ppoll
        fds = StraceArgs.pollfd! args
        if (rc > 0)
          rds = StraceArgs.pollfd! ret.slice(1..-2)
          rds.each{|rd| accumulate pstat.hmap, rd, :wait, duration/rds.size }
        else
          fds.each{|f| accumulate pstat.hmap, f, :wait, duration/fds.size }
        end

      when :epoll_ctl
        epfd = StraceArgs.fd! args
        op = StraceArgs.op! args
        fd  = StraceArgs.fd! args
        case op
        when :EPOLL_CTL_ADD, :EPOLL_CTL_MOD
          pstat.emap[epfd].add fd
        when :EPOLL_CTL_DEL
          pstat.emap[epfd].delete fd
        end

      when :epoll_wait, :epoll_pwait
        epfd = StraceArgs.fd! args
        if rc < 1
          pstat.emap[epfd].each do |f|
            accumulate pstat.hmap, f, :wait, duration/pstat.emap[epfd].size
          end
        else
          epfds = StraceArgs.epollevents! args
          epfds.each{|f| accumulate pstat.hmap, f, :wait, duration/epfds.size }
        end

      when :pread, :read, :recvfrom, :lseek, :recvmsg, :getdents
        fd = StraceArgs.fd! args
        accumulate pstat.hmap, fd, :read, duration

      when :truncate
        file = StraceArgs.name! args
        absfile = File.absolute_path(file, pstat.cwd)
        @iomap[absfile].acc! :write, duration

      when :pwrite, :write, :writev, :send, :sendmsg, :ftruncate
        fd = StraceArgs.fd! args
        accumulate pstat.hmap, fd, :write, duration

      when :sendto  # TODO the case when a sockaddr is specified
        fd = StraceArgs.fd! args
        accumulate pstat.hmap, fd, :write, duration

      when :sendfile
        out_fd = StraceArgs.fd! args
        in_fd = StraceArgs.fd! args
        accumulate pstat.hmap, out_fd, :write, duration/2.0
        accumulate pstat.hmap, in_fd, :write, duration/2.0

      when :fadvise64
        fd = StraceArgs.fd! args
        accumulate pstat.hmap, fd, :other, duration

      when :lgetxattr, :getxattr
        file = StraceArgs.name! args
        path = File.absolute_path(file, pstat.cwd)
        @iomap[path].acc! :read, duration

      when :mmap
        StraceArgs.nextcomma! args
        StraceArgs.nextcomma! args
        StraceArgs.nextcomma! args
        StraceArgs.nextcomma! args
        fd = StraceArgs.fd! args
	if fd == -1
          @dregs += duration
	else
	  accumulate pstat.hmap, fd, :other, duration
	end

      when :arch_prctl, :wait4
        @dregs += duration

      else
        warn "Unhandled call #{call}"
        return 1
      end
      @calls[call][0] += 1
      @calls[call][1] += duration
      return 0
    end
  end

end

# vim: sts=2 sw=2 ts=8
