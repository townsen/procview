class Numeric
  L = 1_000
  S = 1_000 * L
  M = 60 * S
  H = 60 * M
  D = 24 * H
  # Transform a number of seconds with SI suffixes
  def to_duration( max_digits=3 )
    usec = self.to_f.abs*1000000
    return "-" if usec == 0
    value, suffix = case usec
      when 0...L then [ usec, 'µS' ]
      when L...S then [ usec / L, 'mS' ]
      when S...M then [ usec / S, 'S' ]
      when M...H then [ usec / M, 'm' ]
      when H...D then [ usec / H, 'h' ]
      else            [ usec / D, 'd' ]
    end
    used_digits = case value
      when   0...10   then 1
      when  10...100  then 2
      when 100...1000 then 3
    end
    precision = max_digits - used_digits
    precision = 0 if precision < 0
    "#{self < 0 ? '-':''}%.#{precision}f#{suffix}" % value
  end

  # Print a number with SI suffixes to the required precision
  Kilo = 10**3
  Mega = 10**6
  Giga = 10**9
  Tera = 10**12
  Peta = 10**15
  Exa = 10**18
  # Zetta = 10**21
  # Yotta = 10**24

  def to_si( sig_digits=3 )
    raise ArgumentError.new("sig_digits must be > 0") unless sig_digits > 0
    count = self.to_f.abs
    value, suffix = case count
      when 0...Kilo then [ count, '' ]
      when Kilo...Mega then [ count / Kilo, 'k' ]
      when Mega...Giga then [ count / Mega, 'M' ]
      when Giga...Tera then [ count / Giga, 'G' ]
      when Tera...Peta then [ count / Tera, 'T' ]
      when Peta...Exa then [ count / Peta, 'P' ]
      else            [ count / Exa, 'E' ]
    end
    used_digits = case value
      when   0...10   then 1
      when  10...100  then 2
      when 100...1001 then 3
    end
    precision = sig_digits - used_digits
    precision = 0 if precision < 0
    "#{self < 0 ? '-':''}%.#{precision}f#{suffix}" % value
  end
end

module Procview

    # From https://en.wikipedia.org/wiki/Algorithms_for_calculating_variance
    # The "On-line (Welford) algorithm...
    class Accumulator
	attr_reader :n,:ave,:q
        def initialize
	    @n = 0
	    @ave = 0.0
	    @q = 0.0
	end
        def acc! duration
	    @n += 1
	    ak = @ave + (duration - @ave)/@n
	    @q += (duration - @ave)*(duration - ak)
	    @ave = ak
        end
	def sum
	   return @n * @ave
	end
	def var
	   return (@n == 0) ? 0.0 : @q/@n
	end
	def stddev
	   return Math.sqrt(var)
	end
        # Together with Chan's Parallel interpretation
        def +(other)
          return self unless (self.n + other.n) > 0
          delta = other.ave - self.ave
          m_a = self.var * (self.n - 1)
          m_b = other.var * (other.n - 1)
          m2 = m_a + m_b + (delta ** 2) * self.n * other.n / (self.n + other.n)
          newvar = m2 / (self.n + other.n - 1)
          @ave = (self.sum + other.sum) / (@n + other.n)
          @n += other.n
          @q = newvar * @n
          return self
        end
    end

    class Stats
        TYPE = { other: 0, wait: 1, read: 2, write: 3 }.freeze
        attr_reader :accumulators
        protected :accumulators

        def initialize
            @accumulators = 4.times.map{ Accumulator.new }
        end
        def [](y)
            @accumulators[y].sum
        end
        def values
            @accumulators.map{|a|a.sum}
        end
        def add!(other)
            @accumulators.map!.with_index{|t,i| t += other.accumulators[i]}
        end
        def sum
            @accumulators.map{|m|m.sum}.reduce(:+)
        end
        def acc! type, duration
            @accumulators[TYPE[type]].acc! duration
        end
        def durations digits
            @accumulators.map{|a|a.sum.to_duration(digits)}
        end
        def counts digits
            @accumulators.map{|a|a.n.to_si(digits)}
        end
        def averages digits
            @accumulators.map{|a|a.ave.to_duration(digits)}
        end
        def deviations digits
            @accumulators.map{|a|a.stddev.to_duration(digits)}
        end
    end
end


# vim: set ts=8 sw=2 sts=2:
