# Parse the arguments of an Strace call
# Calls with ! affect their arguments
# They parse the input string removing the matching items from the front
# The return the items and modify the input string for easy chaining.
#
module Procview

    module StraceArgs

    STRUCT = /\A(?<b>{([^{}]|\g<b>)*}),?\s*/ # gobble up the largest matching braces

    def self.pollfd! args
        return [] if args.slice!(/^0,\s*/)
        raise "parse error: #{args}" unless args.slice!(/^\[{(.*?)}\],?\s*/)
        $~[1].split(/},{/).map{|m| /^fd=(\d+)/ =~ m; $1.to_i}
    end

    def self.nextcomma! args
        raise "parse error: #{args}" unless args.slice!(/^([^,]+),?\s*/)
        $~[1]
    end

    def self.fd! args
        raise "parse error: #{args}" unless args.slice!(/^(-?\d+),?\s*/)
        $~[1].to_i
    end

    def self.afds! args
        raise "parse error: #{args}" unless args.slice!(/^(\[.*?\]|NULL),?\s*/)
        afd = $~[1]
        return [] if afd.start_with? 'NULL'
        afd.slice(1..-2).split(/\s*,\s*/).map(&:to_i)
    end

    def self.name! args
        raise "parse error: #{args}" unless args.slice!(/^"(.*?)",?\s*/)
        return $~[1]
    end

    def self.op! args
        raise "parse error: #{args}" unless args.slice!(/^(\w+),?\s*/)
        return $~[1].to_sym
    end

    def self.sockaddr! args
        if args.slice!(/^(0|NULL),?\s*/)
            return nil
        end
        raise "parse error: #{args}" unless args.slice! STRUCT
        soa = $~[1].slice(1..-2)
        if /^
            sa_family=AF_INET,\s*
            sin_port=htons\((?<port>\d+)\),\s*
            sin_addr=inet_addr\("(?<host>.*)"\)
            /x =~ soa
            return "#{host}:#{port}"
        elsif /^
            sa_family=AF_INET6,\s*
            sin6_port=htons\((?<port6>\d+)\),\s*
            inet_pton\(AF_INET6,\s*"(?<host6>.*)",\s*\&sin6_addr\),\s*
            sin6_flowinfo=\d+,\s*sin6_scope_id=\d+
            /x =~ soa
            return "#{host6}:#{port6}"
        elsif /^
            sa_family=AF_FILE,\s*
            path="(?<path>.*)"
            /x =~ soa
            return "file:#{path}"
        elsif /^
            sa_family=AF_LOCAL,\s*
            sun_path="(?<sunpath>.*)"
            /x =~ soa
            return "unix:#{sunpath}"
        elsif /^
            sa_family=AF_NETLINK,\s*
            pid=(?<pid>\d+),\s*
            groups=(?<groups>.*)
            /x =~ soa
            return "netlink:#{pid}:#{groups}"
        elsif /^
            sa_family=AF_UNSPEC,\s*
            sa_data="(?<data>.*)"
            /x =~ soa
            return "unspec:#{data}"
        else
            raise "Invalid sockaddr format: #{soa}"
        end
    end

    def self.epollevents! args
        raise "parse error: #{args}" unless args.slice! STRUCT
        evs = $~[1].slice(1..-2)
        rex = /{(.*?),\s{u32=(\d+),\su64=(\d+)}},?\s*/
        eva = []
        while ev = evs.slice!(rex)
            eva.push $~[3].to_i
        end
        raise "parse leftover #{evs}" unless evs.empty?
        return eva
    end

    def self.flags! args
        raise "parse error: #{args}" unless args.slice!(/^\w+=([^,]+),\s*/)
        flags = $~[1].split('|').map{|f| f.to_sym}
        return flags
    end

    end
end
