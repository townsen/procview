require 'fileutils'
require 'procview'
require 'procview/analyzer'
require 'procview/options'

class Float
  def near?(another_float,delta=0.01)
    self.abs - another_float.abs < delta
  end
end

module Procview
  class Runner
    attr_reader :opts

    FORMAT = "%7s"

    def initialize(argv)
      @opts = Options.new(argv)
    end

    def run
      if @opts.debug
        @opts.debug = File.open("/tmp/procview.debug.#{$$}", "w")
        $stderr.puts "Writing debug output to file #{@opts.debug.path}"
      end

      unless @opts.filename
        Procview.checklsof @opts.quiet
        Procview.checkstrace @opts.quiet
      end

      pva = Analyzer.new @opts.to_h

      if @opts.filename
        file = File.open(@opts.filename, "r")
        unless file
          $stderr.puts "Can't open input file #{@opts.filename}"
          exit 2
        end
        if @opts.raw
          pva.pwd = FileUtils.pwd
          pva.inithandles = { 0 => :stdin, 1 => :stdout, 2 => :stderr }
        end
        pva.load(file)
      else
        fifoname = "/tmp/procview.fifo.#{$$}"
        res = `mkfifo #{fifoname}`
        begin
          cmdargs = Procview::STRACE + [ "-o", fifoname ]
          if @opts.pid
            puts "Tracing PID #{@opts.pid} (Ctrl-C to disconnect)"
            cmdargs += [ "-p", @opts.pid.to_s ]
          else
            cmdargs += ARGV
          end
          stpid = spawn(*cmdargs)
          trap 'INT' do
            Process.kill("INT", stpid)
            puts "Sent #{stpid} SIGINT"
          end
          if @opts.pid
            IO.popen(Procview::LSOF + ["-p", @opts.pid.to_s]) do |lsof|
              while line = lsof.gets
                fields = line.split(/\s+/)
                next unless fields[3] =~ /(\d+\w|cwd)/
                @opts.debug.puts "LSOF #{line}\n" if @opts.debug
                pva.digest "LSOF #{line}"
              end
            end
          else
            pva.pwd = FileUtils.pwd
            pva.inithandles = { 0 => :stdin, 1 => :stdout, 2 => :stderr }
            # We don't know the actual traced process PID so put -1
            # and let the analyzer work things out
            @opts.debug.puts <<-FAKELSOF.gsub(/^\s+/,'')  if @opts.debug
              LSOF bash    -1 root  cwd    DIR  253,0     4096 655363 #{pva.pwd}
              LSOF bash    -1 root    0u   CHR  136,0      0t0      3 stdin
              LSOF bash    -1 root    1u   CHR  136,0      0t0      3 stdout
              LSOF bash    -1 root    2u   CHR  136,0      0t0      3 stderr
              FAKELSOF
          end
          File.open(fifoname, "r") do |fifo|
            pva.load(fifo)
          end
          Process.wait(stpid)
        ensure
          FileUtils.rm_f fifoname
        end
      end

      totalBySource = printReport pva
      totalByCall   = pva.calls.values.inject(0.0){|s,v| s + v[1]}

      unless @opts.quiet
        $stderr.puts "Warnings (#{pva.warnings.size})\n" if pva.warnings.size > 0
        pva.warnings.each { |w| $stderr.puts w }
      end

      unless totalByCall.near? totalBySource + pva.dregs, 0.001
        puts "WARNING IO and call totals don't add up!"
        printf "\tDelta %s (The dregs were %s)\n", (totalByCall - totalBySource).to_duration(@opts.digits), pva.dregs.to_duration(@opts.digits)
      end

      printCalls(pva) if @opts.calls

    end

    def mkTitleFormat
      format = FORMAT.dup
      format.prepend " " * 7 if @opts.counts
      format += " " * 7 if @opts.variances
      Array.new(4, format).join(' ') + " %s\n"
    end

    def printLine name, stat
          (0..3).each do |ix|
            printf "%5s @", stat.counts(1)[ix] if @opts.counts
            printf "#{FORMAT}", stat.durations(@opts.digits)[ix]
            printf "±#{FORMAT}", stat.deviations(@opts.digits)[ix] if @opts.variances
            printf " "
          end
          puts name # as it may contain %
    end

    def printReport pva

      titleformat = mkTitleFormat

      puts "Duration by Source"
      printf titleformat, 'OTHER', 'WAIT', 'READ', 'WRITE', 'Name'
      totalSource = Procview::Stats.new
      pva.iomap.keys.sort{|a,b| pva.iomap[b].sum <=> pva.iomap[a].sum}.each do |k|
        binned = false
        @opts.buckets.values.each do |v|
          binned = k =~ v[0]
          if binned
            v[1].add! pva.iomap[k]
            break
          end
        end
        unless binned
          printLine k, pva.iomap[k]
        end
        totalSource.add! pva.iomap[k]
      end
      @opts.buckets.each do |k,v|
        name = k.is_a?(Symbol) ? k.to_s: "Sum: #{k}"
        printLine name, v[1]
      end
      printLine "Total Duration", totalSource
      printf titleformat, pva.dregs.to_duration(@opts.digits),"","","", 'Unassigned'

      return totalSource.sum

    end

    def printCalls pva
      puts "Duration by Call Type"
      totalType = 0.0
      pva.calls.keys.sort{|a,b| pva.calls[b][1] <=> pva.calls[a][1]}.each do |k|
        v = pva.calls[k]
        printf "#{FORMAT} #{k}: #{v[0]}\n", v[1].to_duration(@opts.digits)
        totalType += v[1]
      end
      printf "#{FORMAT} Total duration \n\n", totalType.to_duration(@opts.digits)

      if pva.sigmap.size > 0
        puts "Signals"
        pva.sigmap.keys.sort{|a,b| pva.sigmap[b] <=> pva.sigmap[a]}.each do |k|
          puts "\t#{k}: #{pva.sigmap[k]}"
        end
      end
    end

  end

end

# vim: sts=2 sw=2 ts=8
