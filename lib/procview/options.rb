require 'optparse'
require 'ostruct'
require 'singleton'

require 'procview/stats'
require 'procview/version'

module Procview
  class Options < OpenStruct

    SYSRE = %r{^/(usr/(lib64|share|bin|sbin|tmp)|lib64|proc|etc|usr|var|sbin|bin|dev|selinux|$)}

    DEFAULTS = {
      buckets: {},
      calls: false,
      debug: false,
      digits: 3,
      quiet: false,
      raw: false,
      missing: true,
      sys: true,
      counts: false,
      variances: false
    } 
    def initialize argv
      super(DEFAULTS)

      OptionParser.new do |o|
        o.banner = "usage: #{o.program_name} [options] [-- command [args...]]"
        o.separator "Analyze a Linux strace file. Options (v#{VERSION}):"

        o.on('-f', '--file FILE', "Analyze strace file only") do |f|
          self.filename = f
        end
        o.on("--counts", "Show counts (default: #{self.counts})") do |c|
          self.counts = c
        end
        o.on("--variances", "Show standard deviations (default: #{self.variances})") do |c|
          self.variances = c
        end
        o.on('--digits NUM', Integer, "Number of digits (default: #{self.digits})") do |d|
          self.digits = d
        end
        o.on('-p', '--pid PID', Integer, "Process ID to examine") do |p|
          self.pid = p
        end
        o.on('-s', '--system', "Summarize system files") do
          self.buckets[:'System files'] = [SYSRE, Stats.new]
        end
        o.on('--sum [name=]pattern', "Summarize patterns") do |sum|
          if sum =~/^(\w+)=(.*)$/
            self.buckets[$1] = [Regexp.new($2), Stats.new]
          else
            self.buckets[self.buckets.keys.size] = [Regexp.new(sum), Stats.new]
          end
        end
        o.on('-F', "Summarize all disk file activity") do
          self.buckets[:'All files'] = [%r{/.*}, Stats.new]
        end
        o.on('-c', "--calls", "Show calls and signals (default: #{self.calls})") do |c|
          self.calls = c
        end
        o.on('-m', "--[no-]missing", "Ignore missing files in open() and stat() calls (default: #{self.missing})") do |m|
          self.missing = m
        end
        o.on('-q', '--quiet', "Quiet - no errors (default: #{self.quiet})") do |q|
          self.quiet = q
        end
        o.on('-r', '--raw', "Raw trace - assume cwd and stdio (default: #{self.raw})") do |r|
          self.raw = r
        end
        o.on('-d', '--debug', "Debug - write trace to /tmp (default: #{self.debug})") do |d|
          self.debug = d
        end
        o.on_tail( '-?', '--help', 'Display this screen' ) do
          STDERR.puts o
          exit
        end
        o.parse!(argv) rescue (STDERR.puts "#{o.program_name}: #{$!}\n#{o.to_s}"; exit)
        if self.pid.nil? and self.filename.nil? and argv.size == 0
          STDERR.puts o.help
          exit 2
        end
      end
    end
  end
end

# vim: sts=2 sw=2 ts=8
