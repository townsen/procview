# Examine a trace file and report timings
#
module Procview

  STRACE  = %w{ /usr/bin/env strace -Tfq -e desc,network,file,process }
  LSOF    = %w{ /usr/bin/env lsof }

  def self.checklsof quiet = false
    lsof = `#{(Procview::LSOF+%w{ -v 2>&1 }).join(' ')}`
    /\s+revision:\s+(?<rev>[.\d]+)\s+/ =~ lsof
    raise "'lsof' not found! Check that it is installed and on your PATH" if rev.nil?
    STDERR.puts "Using lsof #{rev}" unless quiet
  end

  def self.checkstrace quiet = false
    strace = `#{(Procview::STRACE+%w{ -V 2>&1 }).join(' ')}`
    /^strace\s+--\s+version\s+(?<rev>[.\d]+)$/ =~ strace
    raise "'strace' not found! Check that it is installed and on your PATH" if rev.nil?
    STDERR.puts "Using strace #{rev}" unless quiet
  end

end

# vim: sts=2 sw=2 ts=8
