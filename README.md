# procview

A tool to determine what I/O a Linux process is doing by interpreting the output
of `strace`. When attaching to an existing process it also uses `lsof` to determine
which file handles are currently open.

The [codebase](https://github.com/townsen/procview.git) also includes an Apache
module (Apache2::Procview) that can be used to trigger a trace on individual web requests.

It is a linux-only GEM as it requires the following Linux programs:

* `lsof` at version 4.82 or higher
* `strace` at version 4.5.19

## Installation
This tool is published to [RubyGems](https://rubygems.org) so you can simply do:

    gem install procview

## Usage
This tool can be used in one of three ways:

* To analyze a currently running process in realtime (using the `-p` option)
* To run a command while performing the analysis (by supplying a command)
* To post-process trace files produced by `Apache2::Procview` or a raw `strace`

To get a summary of the options type `procview -?`

### Manual `strace`

Create a trace file for later analysis using the following command:

    strace -Tfq -e desc,network,file,process -o trace.file [either -p or command+args]

## Output
The output of procview lists the I/O endpoints and the total time recorded
against them in four categories: read, write, wait and other. The read and write
times mean the obvious. The wait time is time spent in calls like poll and
select. Other time is any time that doesn't belong to that. For example the
duration of a `chdir('/directory')` call accumulates into the 'other' category
for the '/directory' path.

## Summarization Options

The summary options all create 'buckets' for the accumulation of statistics.
Each bucket has a patternset that is used to match against the name associated
with each I/O. The statistics are accumulated in the first bucket found.  The
sequence of buckets is as they appear on the command line.

*  `-F`: Summarize all disk file activity in one line.
*  `-s`: Summarize all activity to the LSB 'system' directories
*  `--sum`: Create a bucket using a regular expression.
   The bucket can optionally be named by included `name=` prior to the regular
expression. The RE syntax is that of Ruby.

### Unassigned
There are certain calls which cannot be allocated against any IO handles.
Examples would be stat() or close() calls for non-existant handles. These are
accumulated into a special bucket called 'dregs' and displayed as _Unassigned_
in the output. This time is accounted for in the call level summary, but cannot
be placed into an I/O bucket.

### Calls
Optionally the breakdown of time per _call_ can be displayed. If this total
doesn't match the breakdown by name __plus__ the 'unassigned' time then that's a
bug and should be reported!

## Problems and Known Issues
There are many potential calls produced by `strace` and procview is not
guaranteed to handle them all.  In such cases the utility will print an error
message to stderr and continue. Record the output and create an Issue on GitHub.
