$:.push File.expand_path("../lib", __FILE__)

require 'procview/version'

Procview::GemSpec = Gem::Specification.new do |s|
  s.name        = %q{procview}
  s.version     = Procview::VERSION
  s.required_ruby_version = ">=1.9.2"
  s.platform    = Gem::Platform::CURRENT
  s.date        = Time.now.strftime("%Y-%m-%d")
  s.authors     = ['Nick Townsend']
  s.email       = ['nick.townsend@mac.com']
  s.summary     = %q{What is a process doing?}
  s.homepage    = %q{https://github.com/townsen/procview/}
  s.extra_rdoc_files  = ['README.md','CHANGELOG.md']
  s.rdoc_options << '--title' << 'Procview - IO analyzer' << '--main' << 'README.md'
  s.rdoc_options << '-x' << 'gem_make.out' << '-x' << 'gem.build_comp'
  s.files       = `git ls-files`.split($/).grep(%r{^(?!apache)})
  s.test_files  = s.files.grep(%r{^test})
  s.description = "Determine what a process is doing using strace and lsof. Linux only."
  s.executables = [ "procview" ]
  s.extensions  = ["ext/Rakefile"]
  s.license     = 'MIT'
  s.requirements << 'The lsof and strace packages'
  s.add_development_dependency 'minitest', '~> 4.3', '>= 4.3.2'
  s.add_development_dependency 'rake-compiler', '~> 0.9', '>= 0.9.2'
  s.add_development_dependency 'pry'
  s.add_development_dependency 'pry-byebug'
end
