require 'minitest/autorun'
require 'procview/stats'

class TestStats < MiniTest::Unit::TestCase
  def test_to_duration
    assert_equal("-", 0.to_duration(0))
    assert_equal("1µS", (0.000001).to_duration(0))
    assert_equal("-", 0.to_duration(2))
    assert_equal("3mS", (0.003).to_duration(0))
    assert_equal("3.00mS", (0.003).to_duration)
    assert_equal("1m", 60.to_duration(1))
    assert_equal("1m", 89.999999999999.to_duration(0))
    assert_equal("1.5m", 90.to_duration(2))
    assert_equal("2m", 90.to_duration(1))
    assert_equal("1h", 3600.to_duration(1))
    assert_equal("2.1d", (86400*2.1).to_duration(2))
    assert_equal("-2S", -2.to_duration(0))
    assert_equal("-19d", (-19*86400).to_duration(1))
  end

  def test_accumulation
    a = Procview::Accumulator.new
    values = [2,4,4,4,5,5,7,9]
    values.each{|v| a.acc!(v)}
    assert_equal(values.size,a.n)
    assert_equal(5.0,a.ave, "ave()")
    assert_equal(40.0, a.sum, "sum()")
    assert_in_epsilon(2.0, a.stddev, 0.01, "stddev()")
    b = Procview::Accumulator.new
    values.each{|v| b.acc!(2 * v)}
    assert_equal(10.0,b.ave, "double ave()")
    assert_equal(80.0, b.sum, "double sum()")
    assert_in_epsilon(4.0, b.stddev, 0.01, "double stddev()")
  end

  def test_nil_accumulation
    a = Procview::Accumulator.new
    assert_equal(0,a.n, "N()")
    assert_equal(0.0,a.ave, "ave()")
    assert_equal(0.0, a.sum, "sum()")
    assert_equal(0.0, a.stddev, "stddev()")
    b = Procview::Accumulator.new
    a += b
    assert_equal(0,a.n, "N()")
  end

  def test_add_accumulations
    a = Procview::Accumulator.new
    [2,4,4,4].each{|v| a.acc!(v)}
    b = Procview::Accumulator.new
    [5,5,7,9].each{|v| b.acc!(v)}
    a += b
    assert_equal(8,a.n,"parallel N")
    assert_equal(5.0,a.ave, "parallel ave()")
    assert_equal(40.0, a.sum, "parallel sum()")
    assert_in_epsilon(2.0, a.stddev, 0.01, "parallel stddev()")
  end

  def test_stats
    s = Procview::Stats.new
    assert_raises TypeError do
      s.acc! :random, 1.0
    end
    s.acc! :other, 1.0
    s.acc! :wait, 2.0
    s.acc! :read, 3.0
    s.acc! :write, 4.0
    assert_equal(3.0, s[2], "Stat.[]")
    assert_equal(10.0, s.sum, "Stat.sum()")
    assert_equal([1.0,2.0,3.0,4.0], s.values, "values()")
    t = s.dup
    s.add! t
    assert_equal([2.0,4.0,6.0,8.0], s.values, "add!()")
  end

  def test_empty_stats
    s = Procview::Stats.new
    s.acc! :other, 1.0
    t = Procview::Stats.new
    s.add! t
    assert_equal([1.0,0.0,0.0,0.0], s.values, "add!()")
  end

end
#
# vim: set ts=8 sw=2 sts=2:
