# Simple tests
#
require 'bundler/setup'
require 'minitest/autorun'
require 'procview/straceargs'

describe Procview::StraceArgs do
    describe "general argument parsing" do
        it "handles op" do
            args = "AF_INET, SOCK_STREAM, TCP"
            definition = Procview::StraceArgs.op! args
            definition.must_equal :AF_INET
            args.must_equal "SOCK_STREAM, TCP"
        end
        it "handles filename" do
            args = '"/usr/bin/nick", 8, NULL'
            name = Procview::StraceArgs.name! args
            name.must_equal "/usr/bin/nick"
            args.must_equal "8, NULL"
        end
        it "handles skipping" do
            args = "child_stack=0x7f2a6a666fd0, flags=CLONE_VM, tls=0x7f2a6a667700"
            arg = Procview::StraceArgs.nextcomma! args
            arg.must_equal "child_stack=0x7f2a6a666fd0"
            args.must_equal "flags=CLONE_VM, tls=0x7f2a6a667700"
        end
        it "handles skipping at end" do
            args = "MAP_PRIVATE|MAP_ANONYMOUS, -1, 0"
            arg = Procview::StraceArgs.nextcomma! args
            arg.must_equal "MAP_PRIVATE|MAP_ANONYMOUS"
            args.must_equal "-1, 0"
        end
    end
    describe "fd parsing" do
        it "handles a single fd" do
            args = "8, NULL, NULL, NULL, {60, 0}"
            fd = Procview::StraceArgs.fd! args
            fd.must_equal 8
            args.must_equal "NULL, NULL, NULL, {60, 0}"
        end
        it "handles a bad fd" do
            args = "NULL, {60, 0}"
            proc {
                fd = Procview::StraceArgs.fd! args
            }.must_raise RuntimeError
        end
        it "handles an array of one fd" do
            args = "[6], NULL, NULL, NULL, {60, 0}"
            afds = Procview::StraceArgs.afds! args
            afds.must_include 6
            args.must_equal "NULL, NULL, NULL, {60, 0}"
        end
        it "handles multiple fds" do
            args = "[5,6,7], NULL, NULL, {60, 0}"
            afds = Procview::StraceArgs.afds! args
            afds.must_include 5
            afds.must_include 6
            afds.must_include 7
            args.must_equal "NULL, NULL, {60, 0}"
        end
    end
    describe "pollfd struct parsing" do
        it "handles an empty pollfd" do
            args = "0, 0, 4"
            fds = Procview::StraceArgs.pollfd! args
            fds.size.must_equal 0
            args.must_equal "0, 4"
        end
        it "handles a single pollfd" do
            args = "[{fd=3, events=POLLIN|POLLRDNORM}], 1, 0"
            fds = Procview::StraceArgs.pollfd! args
            fds.size.must_equal 1
            fds.must_include 3
            args.must_equal "1, 0"
        end
        it "handles multiple pollfds" do
            args = "[{fd=10, events=POLLIN},{fd=12, events=POLLHUP}], 2, 1"
            fds = Procview::StraceArgs.pollfd! args
            fds.size.must_equal 2
            fds.must_include 10
            fds.must_include 12
            args.must_equal "2, 1"
        end
        it "handles returned pollfds" do
            args = "[{fd=10, revents=POLLIN},{fd=12, revents=POLLHUP}], 2, 1"
            fds = Procview::StraceArgs.pollfd! args
            fds.size.must_equal 2
            fds.must_include 10
            fds.must_include 12
            args.must_equal "2, 1"
        end
    end
    describe "sockaddr parsing" do
        it "handles AF_INET" do
            arg = %q#{sa_family=AF_INET, sin_port=htons(80), sin_addr=inet_addr("108.178.62.202")}#
            name = Procview::StraceArgs.sockaddr! arg
            name.must_equal "108.178.62.202:80"
            arg.must_be_empty
        end
        it "handles AF_FILE" do
            arg = %q#{sa_family=AF_FILE, path="/var/run/nscd/socket"}, NULL#
            name = Procview::StraceArgs.sockaddr! arg
            name.must_equal "file:/var/run/nscd/socket"
            arg.must_equal "NULL"
        end
        it "handles AF_LOCAL" do
            arg = %q#{sa_family=AF_LOCAL, sun_path="/var/run/nscd/socket"}, 110#
            name = Procview::StraceArgs.sockaddr! arg
            name.must_equal "unix:/var/run/nscd/socket"
            arg.must_equal "110"
        end
        it "handles AF_NETLINK" do
            arg = %q#{sa_family=AF_NETLINK, pid=0, groups=00000000}, 12#
            name = Procview::StraceArgs.sockaddr! arg
            name.must_equal "netlink:0:00000000"
            arg.must_equal "12"
        end
        it "handles AF_INET6" do
            arg = %q#{sa_family=AF_INET6, sin6_port=htons(80), inet_pton(AF_INET6, "::1", &sin6_addr), sin6_flowinfo=0, sin6_scope_id=0}, 28#
            name = Procview::StraceArgs.sockaddr! arg
            name.must_equal "::1:80"
            arg.must_equal "28"
        end
        it "handles AF_UNSPEC" do
            arg = %q#{sa_family=AF_UNSPEC, sa_data="\0\0\0\0\0\0\0\0\0\0\0\0\0\0"}, 16#
            name = Procview::StraceArgs.sockaddr! arg
            name.must_equal 'unspec:\0\0\0\0\0\0\0\0\0\0\0\0\0\0'
            arg.must_equal "16"
        end
        it "fails when unparseable AF" do
            proc {
                Procview::StraceArgs.sockaddr! %q#{sa_family=AF_UNSPEC}#
            }.must_raise RuntimeError
        end
    end
    describe "epoll parsing" do
        it "handles empty" do
            arg = "{}"
            events = Procview::StraceArgs.epollevents! arg
            events.size.must_equal 0
            arg.must_be_empty
        end
        it "leaves trailing args" do
            arg = "{}, 5, 6"
            events = Procview::StraceArgs.epollevents! arg
            events.size.must_equal 0
            arg.must_equal "5, 6"
        end
        it "rejects malformed args" do
            arg = "{{}, 5, 6"
            proc {
                events = Procview::StraceArgs.epollevents! arg
            }.must_raise RuntimeError
        end
        it "handles simple input" do
            arg = "{{EPOLLIN, {u32=15, u64=15}}}"
            events = Procview::StraceArgs.epollevents! arg
            events.size.must_equal 1
            events.must_include 15
            arg.must_be_empty
        end
        it "handles complex input" do
            arg = "{{EPOLLIN, {u32=12, u64=12}},{EPOLLIN, {u32=15, u64=15}}}"
            events = Procview::StraceArgs.epollevents! arg
            events.size.must_equal 2
            events.must_include 12
            events.must_include 15
            arg.must_be_empty
        end
        it "handles more complex input" do
            arg = '{{EPOLLIN|0x2000, {u32=16, u64=16}}, {EPOLLIN|0x2000, {u32=18, u64=18}}}, 5000, 1000'
            events = Procview::StraceArgs.epollevents! arg
            arg.must_equal '5000, 1000'
        end
        it "handles flags input" do
            arg = "{{EPOLLIN|0x2000, {u32=15, u64=15}}}, 5000, 1000"
            events = Procview::StraceArgs.epollevents! arg
            events.must_include 15
            arg.must_equal "5000, 1000"
        end
    end
    describe "bitflag parsing" do
        it "handles clone" do
            args = "flags=CLONE_VM|CLONE_FS|CLONE_CHILD_CLEARTID, tls=0x7f2a6a6679d0"
            flags = Procview::StraceArgs.flags! args
            flags.must_include :CLONE_FS
            flags.wont_include :CLONE_THREAD
        end
    end
end
