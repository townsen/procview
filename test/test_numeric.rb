# Do the Spec style for a change
#
require 'minitest/autorun'
require 'procview/stats'

describe Numeric do
  describe "to_si" do
    it "must validate the maximum digits" do
      proc { 0.to_si(0) }.must_raise ArgumentError
    end
    it "must work" do
      0.to_si(1).must_equal "0"
      0.to_si(2).must_equal "0.0"
      0.to_si(3).must_equal "0.00"
      1100.to_si(2).must_equal "1.1k"
    end
    it "must format nicely" do
      101.to_si(1).must_equal "101"
      101.to_si(2).must_equal "101"
      101.to_si(3).must_equal "101"
    end
    it "must handle rounding" do
      1023.to_si(1).must_equal "1k"
      1023.to_si(2).must_equal "1.0k"
      1499.to_si(1).must_equal "1k"
      1500.to_si(1).must_equal "2k"
      1501.to_si(1).must_equal "2k"
      1823.to_si(1).must_equal "2k"
      1823.to_si(2).must_equal "1.8k"
    end
    it "must handle big numbers" do
      3_000_000.to_si(1).must_equal "3M"
      3_000_000.to_si(3).must_equal "3.00M"
      2_162_988_919.to_si(4).must_equal "2.163G"
    end
  end
end
