# Special Case tests
#
require 'bundler/setup'
require 'minitest/autorun'
require 'procview/analyzer'

describe Procview::Analyzer do
    before do
        @pv = Procview::Analyzer.new
    end
    def gobble lines
        lines.split(/\n/).each do |line|
            @pv.digest(line.strip).must_equal 0
        end
    end
    describe "unknown handles" do
        it "copes with repolist" do
            gobble <<LINES
5440  socket(PF_INET, SOCK_STREAM, IPPROTO_IP <unfinished ...>
5438  read(20,  <unfinished ...>
5439  open("/etc/hosts", O_RDONLY|O_CLOEXEC <unfinished ...>
5440  <... socket resumed> )            = 21 <0.000104>
5439  <... open resumed> )              = 11 <0.000009>
5439  fstat(11, {st_mode=S_IFREG|0644, st_size=158, ...}) = 0 <0.000004>
LINES
        @pv.warnings.must_be_empty
        end
    end
end
