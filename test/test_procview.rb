# Simple tests
#
require 'bundler/setup'
require 'minitest/autorun'
require 'procview'

describe Procview do
    describe "binary check" do
        it "finds lsof" do
            out, err = capture_subprocess_io do
                Procview.checklsof
            end
            err.must_match(/lsof/)
        end
        it "finds strace" do
            out, err = capture_subprocess_io do
                Procview.checkstrace
            end
            err.must_match(/strace/)
        end
    end
end
