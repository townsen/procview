# Runner tests
#
require 'tempfile'
require 'bundler/setup'
require 'minitest/autorun'
require 'procview/runner'

describe Procview::Runner do
    describe "IO handling" do
        before do
            @file = Tempfile.new('procview.badfile')
            # Bad because it returns 2 but there are no returned events
            @file.write <<TEST
            cuckoo
poll([{fd=3, events=POLLIN}], 1, 0) = 2 (Timeout) <0.000005>
TEST
            @file.close
        end
        it "writes errors to STDERR when quiet flag is false" do
            out, err = capture_io do
                runner = Procview::Runner.new %W(-f #{@file.path} )
                runner.run
            end
            assert_match(/Total Duration/, out)
            assert_match(/parse error: Timeout/, err)
        end
        it "obeys the quiet flag" do
            out, err = capture_io do
                runner = Procview::Runner.new %W(-f #{@file.path} --quiet)
                runner.run
            end
            assert_match( /Total Duration/, out )
            err.must_be_empty
        end
    end
end
