# Simple tests
#
require 'stringio'
require 'bundler/setup'
require 'minitest/autorun'
require 'procview/analyzer'

describe Procview::Analyzer do
    def gobble lines
        lines.split(/\n/).each do |line|
            @pv.digest(line.strip).must_equal 0
        end
    end
    describe "line parsing" do
        before do
            @pv = Procview::Analyzer.new
        end
        it "parses a simple call with no pid" do
            mock = MiniTest::Mock.new
            mock.expect(:handleCall, 0, [:chdir, '"/usr/bin"', 0, '', 0.000005])
            @pv.stub :handleCall, -> (c,a,r,t,d) { mock.handleCall(c,a,r,t,d) } do
                @pv.digest 'chdir("/usr/bin") = 0 <0.000005>'
            end
            mock.verify
        end
        it "handles a call with a pid" do
            @pv.digest "1234 poll([{fd=3, events=POLLIN|POLLRDBAND}], 1, 0) = 0 (Timeout) <0.000005>"
            @pv.calls.must_include :poll
            @pv.pstats.must_include 1234
        end
        it "parses a full call with pid" do
            mock = MiniTest::Mock.new
            mock.expect(:handleCall, 0, [:poll, '[{fd=13, events=POLLIN|POLLOUT|0x2000}], 1, 1000' , 1, '([{fd=13, revents=POLLOUT}])', 0.000008])
            @pv.stub :handleCall, -> (c,a,r,t,d) { mock.handleCall(c,a,r,t,d) } do
                @pv.digest '12804 poll([{fd=13, events=POLLIN|POLLOUT|0x2000}], 1, 1000) = 1 ([{fd=13, revents=POLLOUT}]) <0.000008>'
            end
            mock.verify
        end
        it "handles a bad poll" do
            proc {
                @pv.digest "1234 poll([{fd=3, events=POLLIN}], 1, 0) = 2 (Timeout) <0.000005>"
            }.must_raise RuntimeError
        end
        it "reconstructs a simple unfinished call" do
            mock = MiniTest::Mock.new
            mock.expect(:handleCall, 0, [:select, '4, [3], NULL, NULL, NULL', 1, '(in [3])', 0.000011])
            @pv.stub :handleCall, -> (c,a,r,t,d) { mock.handleCall(c,a,r,t,d) } do
            gobble <<LINES
1234 select(4, [3], NULL, NULL, NULL <unfinished ...>
1234 <... select resumed> )      = 1 (in [3]) <0.000011>
LINES
            end
            mock.verify
        end
        it "copes with interleaved unfinished calls" do
            gobble <<LINES
11460 read(15,  <unfinished ...>
11486 close(15)                         = 0 <0.000005>
11486 fcntl(16, F_SETFD, FD_CLOEXEC)    = 0 <0.000005>
11486 dup2(14, 1)                       = 1 <0.000005>
11486 close(14)                         = 0 <0.000005>
11486 execve("/usr/bin/gem", ["gem", "query", "-r", "--name-matches", "first"], [/* 6 vars */] <unfinished ...>
11460 <... read resumed> "", 4)         = 0 <0.000601>
11460 close(15)                         = 0 <0.000009>
11486 <... execve resumed> "", 4) = 1 <0.000123>
LINES
            @pv.pstats.must_include 11460
            @pv.pstats.must_include 11486
        end
        it "copes with interleaved pipe call" do
            gobble <<-LINES
            10249 pipe( <unfinished ...>
            10248 socket(PF_INET, SOCK_STREAM, IPPROTO_IP <unfinished ...>
            10249 <... pipe resumed> [8, 9])        = 0 <0.000055>
            LINES
            @pv.calls.must_include :pipe
            @pv.pstats[10249].hmap.must_include 8
            @pv.pstats[10249].hmap.must_include 9
            @pv.pstats[10249].hmap[8].must_equal :pipe
        end
        it "ignores info lines" do
            @pv.digest("TIME 2014-01-27 at 19:04:30.858824 on nixvm6i.local").must_equal 0
            @pv.digest("REQUEST GET /perl/dowork.pl?gem=ditto&name=brian&report=10046 HTTP/1.1").must_equal 0
        end
    end
    describe "LSOF processing" do
        before do
            @pv = Procview::Analyzer.new
        end
        it "sets pid from cwd" do
            @pv.digest("LSOF httpd   13708 apache  cwd    DIR  253,0     4096       2 /").must_equal 0
            @pv.ipid.must_equal 13708
            ex = proc {
                @pv.digest("LSOF httpd   13718 apache  cwd    DIR  253,0     4096       2 /")
            }.must_raise RuntimeError
            ex.message.must_match(/PID set prior to cwd/)
        end
        it "records absolute file paths correctly" do
            gobble <<-LINES
            LSOF httpd   11460 apache  cwd    DIR  253,0     4096       2 /random
            11460 open("/util.so", O_RDONLY) = 1 <0.000009>
            LINES
            @pv.iomap.must_include '/util.so'
        end
        it "records relative file paths correctly" do
            gobble <<-LINES
            LSOF httpd   11460 apache  cwd    DIR  253,0     4096       2 /random
            11460 open("./util.so", O_RDONLY) = 1 <0.000009>
            LINES
            @pv.iomap.must_include '/random/util.so'
        end
        it "records file IO correctly" do
            gobble <<-LINES
            LSOF httpd   11460 apache  cwd    DIR  253,0     4096       2 /random
            11460 open("/util.so", O_RDONLY) = 1 <0.000009>
            11460 read(1, "\177ELF\0 &\0\0\0\0\0\0"..., 832) = 832 <0.000006>
            11460 close(1)                          = 0 <0.000005>
            LINES
            @pv.iomap.must_include '/util.so'
            @pv.iomap['/util.so'][0].must_be_within_epsilon 0.000014
            @pv.iomap['/util.so'][2].must_be_within_epsilon 0.000006
        end
    end
end
