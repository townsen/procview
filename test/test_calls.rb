# Simple tests
#
require 'bundler/setup'
require 'minitest/autorun'
require 'procview/analyzer'

describe Procview::Analyzer do
    before do
        @pv = Procview::Analyzer.new
    end
    def gobble lines
        lines.split(/\n/).each do |line|
            @pv.digest(line.strip).must_equal 0
        end
    end
    describe "poll processing" do
        it "copes with empty timeout poll" do
            gobble <<LINES
1987  poll(0, 0, 4)                     = 0 (Timeout) <0.004108>
LINES
        @pv.calls.must_include :poll
        end
        it "copes with ordinary timeout poll" do
            gobble <<LINES
1963  poll([{fd=3, events=POLLIN|POLLRDNORM}], 1, 0) = 0 (Timeout) <0.000007>
LINES
        @pv.calls.must_include :poll
        end
        it "copes with returned events" do
            gobble <<LINES
12804 poll([{fd=13, events=POLLIN|POLLOUT|0x2000}], 1, 1000) = 1 ([{fd=13, revents=POLLOUT}]) <0.000008>
LINES
        @pv.calls.must_include :poll
        end
    end
    describe "epoll processing" do
        it "copes with epoll_wait unfinished" do
            gobble <<LINES
2896  epoll_wait(12, {{EPOLLIN, {u32=15, u64=15}}}, 5000, 1000) = 1 <0.981354>
LINES
        @pv.calls.must_include :epoll_wait
        end
    end
    describe "socket-connect-bind processing" do
        it "socket updates the handle map" do
            gobble <<-LINES
            12502 socket(PF_FILE, SOCK_STREAM|SOCK_NONBLOCK, 0) = 13 <0.000012>
            LINES
            @pv.pstats.must_include 12502
            @pv.pstats[12502].hmap.must_include 13
        end
        it "socket followed by connect moves it to the IO map" do
            name = 'file:/var/run/nscd/socket'
            gobble <<LINES
12502 socket(PF_FILE, SOCK_STREAM|SOCK_NONBLOCK, 0) = 13 <0.000012>
12502 connect(13, {sa_family=AF_FILE, path="/var/run/nscd/socket"}, 110) = -1 ENOENT (No such file or directory) <0.000013>
LINES
            @pv.pstats[12502].hmap[13].must_equal name
            @pv.iomap.must_include name
            @pv.iomap[name][0].must_be_within_epsilon 0.000025
        end
        it "socket followed by connect then close doesn't destroy IO map" do
            name = 'file:/var/run/nscd/socket'
            gobble <<LINES
12502 socket(PF_FILE, SOCK_STREAM|SOCK_NONBLOCK, 0) = 13 <0.000012>
12502 connect(13, {sa_family=AF_FILE, path="/var/run/nscd/socket"}, 110) = -1 ENOENT (No such file or directory) <0.000013>
12502 close(13)                         = 0 <0.000013>
LINES
            @pv.pstats[12502].hmap.wont_include 13
            @pv.iomap.must_include name
            @pv.iomap[name][0].must_be_within_epsilon 0.000038
        end
        it "handles two connects on same socket to same file" do
            name = 'file:/var/run/nscd/socket'
            gobble <<LINES
12502 socket(PF_FILE, SOCK_STREAM|SOCK_NONBLOCK, 0) = 13 <0.000017>
12502 connect(13, {sa_family=AF_FILE, path="/var/run/nscd/socket"}, 110) = -1 ENOENT (No such file or directory) <0.000019>
12502 close(13)                         = 0 <0.000013>
12502 socket(PF_FILE, SOCK_STREAM|SOCK_NONBLOCK, 0) = 13 <0.000012>
12502 connect(13, {sa_family=AF_FILE, path="/var/run/nscd/socket"}, 110) = -1 ENOENT (No such file or directory) <0.000013>
12502 close(13)                         = 0 <0.000012>
LINES
            @pv.iomap.must_include name
            @pv.iomap[name][0].must_be_within_epsilon 0.000086
        end
        it "socket followed by complex connect moves it to the IO map" do
            name = '127.0.0.1:1313'
            gobble <<LINES
12804 socket(PF_INET, SOCK_STREAM, IPPROTO_IP) = 13 <0.000011>
12804 setsockopt(13, SOL_SOCKET, SO_REUSEADDR, [1], 4) = 0 <0.000007>
12804 connect(13, {sa_family=AF_INET, sin_port=htons(1313), sin_addr=inet_addr("127.0.0.1")}, 16) = -1 EINPROGRESS (Operation now in progress) <0.000250>
12804 poll([{fd=13, events=POLLIN|POLLOUT|0x2000}], 1, 1000) = 1 ([{fd=13, revents=POLLOUT}]) <0.000008>
12804 connect(13, {sa_family=AF_INET, sin_port=htons(1313), sin_addr=inet_addr("127.0.0.1")}, 16) = 0 <0.000110>
LINES
            @pv.pstats[12804].hmap[13].must_equal name
            @pv.iomap.must_include name
            @pv.iomap[name][0].must_be_within_epsilon 0.000378
            @pv.iomap[name][1].must_be_within_epsilon 0.000008
        end
    end
    describe "fcntl calls" do
        it "handles F_DUPFD" do
            gobble <<LINES
2809  socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP) = 6 <0.000008>
2809  fcntl(6, F_DUPFD, 20)             = 20 <0.000004>
LINES
            @pv.pstats[2809].hmap.must_include 6
            @pv.pstats[2809].hmap.must_include 20
        end
    end
    describe "misc calls" do
        it "handles eventfd" do
            gobble <<LINES
12345 eventfd2(0, O_NONBLOCK|O_CLOEXEC) = 8 <0.000010>
LINES
            @pv.calls.must_include :eventfd2
            @pv.iomap.must_include :eventfd2
        end
        it "handles accept4 with empty sockaddr" do
            gobble <<LINES
12345 accept4(10, 0, NULL, SOCK_CLOEXEC|SOCK_NONBLOCK) = 11 <0.000027>
LINES
            @pv.calls.must_include :accept4
            @pv.pstats[12345].hmap[11].must_equal 'accept411'
        end
        it "handles bogus close" do
            gobble <<LINES
12345 close(10001) = -1 <0.000027>
LINES
            @pv.calls.must_include :close
            @pv.dregs.must_equal 0.000027
            @pv.pstats[12345].hmap.wont_include 11
        end
        it "handles dup" do
            gobble <<LINES
dup(36)                                 = 26 <0.000018>
LINES
            @pv.calls.must_include :dup
            @pv.pstats[nil].hmap.must_include 26
            @pv.pstats[nil].hmap.must_include 36
            @pv.iomap[:h36][0].must_be_within_epsilon 0.000018
        end
        it "handles utimes" do
            gobble <<LINES
utimes("/var/cache/yum/x86_64/6/stable/repomdzO6L3xtmp.xml", {{1394583731, 0}, {1394583731, 0}}) = 0 <0.00001>
LINES
            @pv.calls.must_include :utimes
        end
    end
end
