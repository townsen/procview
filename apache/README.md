# Overview
This directory contains an Apache 2 handler using `mod_perl` that processes the
request whilst running `lsof` and `strace` on the process.

It writes a temporary file containing the trace information to disk for later
analysis using the [http://github.com/townsen/procview](procview) gem.

# Installation

You will need to install:

* [perl-Linux-Futex](https://github.com/townsen/perl-Linux-Futex)
* [Apache-Scoreboard](http://search.cpan.org/CPAN/authors/id/M/MJ/MJH/Apache-Scoreboard-2.09.2.tar.gz)
* perl-Test-Pod

As root do the following:

    perl Makefile.PL
    make
    make install

You can also build an RPM of this package (see procview/Rakefile)

# Configuration

To use this update the `httpd` configuration file as follows:

Load the module in the server or virtual host context with:

    PerlModule Apache2::Procview
    ExtendedStatus On

Enable the handler in a directory or location section:

    <Location /perl/>
        SetHandler perl-script
        PerlInitHandler Apache2::Procview
        PerlResponseHandler ModPerl::Registry
        PerlOptions +ParseHeaders
        Options +ExecCGI
        allow from all
    </Location>

See the POD documentation in `lib/Apache2/Procview.pm` for more details

# Usage

You'll need both `lsof` and `strace` from the following packages:
* lsof-4.82-4.el6.x86_64
* strace-4.5.19-1.17.el6.x86_64

They should be setuid root in order to get this information from other processes:

    chmod u+s /usr/bin/strace /usr/sbin/lsof

To analyze the traces install the `procview` gem:

    gem install procview
