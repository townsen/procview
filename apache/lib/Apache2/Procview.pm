package Apache2::Procview;
use strict;
use warnings;

=head1 NAME

Apache2-Procview - strace Apache requests and dump the scoreboard

=head1 SYNOPSIS

The module must be loaded in the server or virtual host context with:

	PerlModule Apache2::Procview
	ExtendedStatus On

The handler itself is enabled in a directory or location section:

	PerlInitHandler Apache2::Procview

=head2 OPTIONS

The following Perl variables must be declared at the server level.

To allow tracing to be activated by the presence of the word I<procview>
at the end of the user agent, add this line to the configuration:

	PerlSetVar Apache2-Procview-Useragent 1

To log scoreboard information when the number of child process servers
is at the maximum limit set a non-zero value for this variable:

	PerlSetVar Apache2-Procview-ScoreInterval 30

This represents the minimum number of seconds between logged
scoreboard entries. The default value is 0.

After a scoreboard dump is triggered, a fixed number of the following requests
will automatically be traced. The following variable controls this:

	PerlSetVar Apache2-Procview-MaxTrace 3

The default value of this variable is zero. The count is reset upon the next
scoreboard dump.

To set the path to which the trace files will be written:

    PerlSetVar Apache2-Procview-TracePath /tmp

The default value for this is C</tmp>

=head1 DESCRIPTION

This module performs two distinct functions both of which are logged in the Apache error log:

=over

=item * Dumping the Scoreboard when all Child Processes are used (at the C<alert> level), and

=item * Performing an IO trace on a request using C<lsof> and C<strace> on a request (at the C<notice> level)

=back 

The scoreboard is only dumped when the Score Interval variable is greater than zero and
there are no more child processes available. Since such conditions tend to persist the
dump will only be repeated after Score Interval seconds have elapsed since the last one.

The message for a trace contains the filename: C<< /<TracePath>/procview.<pid>.<sec>.<usec>.trace >>

Although this is plaintext the C<procview> utility should be used to analyze it:

    procview -mf <path-to-trace-file>

Tracing can be disabled dynamically at runtime by creating the file C<< /<TracePath>/DisableProcviewTrace >>

=head1 PROBLEMS

Note that C<lsof> and C<strace> should be marked setuid root.

=head1 AUTHOR

Written by Nick Townsend (github.com/townsen) who wrote Procview (https://github.com/townsen/procview)

Based on Apache2::Instrument Phillipe M. Chiasson L<gozer@apache.org>

Thanks to Fred Moyer L<fred@redhotpenguin.com> for various improvements

Version 0.40 released by Nick Townsend L<nick.townsend@mac.com>

=head1 LICENCE AND COPYRIGHT

Copyright 2014 Nicholas J Townsend

This module is free software; you can redistribute it and/or
modify it under the same terms as Perl itself. See L<perlartistic>.

=cut

our $VERSION = '0.42';

use Apache::Scoreboard ();
use Apache2::Const -compile => qw(OK SERVER_ERROR);
use Apache2::Log ();
use Apache2::Process ();
use Apache2::RequestUtil ();
use Apache2::RequestRec ();
use Apache2::ServerRec ();
use Apache2::ServerUtil ();

use APR::Table ();

use Data::Dumper;
use Linux::Futex ();
use Sys::Hostname;
use Time::HiRes qw(gettimeofday);

use IPC::SysV qw(S_IROTH S_IWOTH S_IRUSR S_IWUSR IPC_CREAT);
use IPC::SharedMem;

Apache2::ServerUtil->server->push_handlers(PerlPostConfigHandler => \&post_config);
Apache2::ServerUtil->server->push_handlers(ChildInit => \&child_init);

use constant SHM_PERMS => S_IRUSR | S_IWUSR | S_IROTH | S_IWOTH;
use constant STRACE => "/usr/bin/strace";
use constant LSOF => "/usr/sbin/lsof";

# $SHM is a handle to 12 bytes of shared memory, used as three 32 bit integers:
# 1. the Futex itself
# 2. the number of requests currently being traced
# 3. the time that the maximum number of children was logged
#
our $SHM;

# Configuration variables
#
our $PATH;		# path to write the trace files to
our $SCORE_INTERVAL;	# time in seconds to wait between dumping scoreboard
our $MAX_TRACE;		# max concurrent traces
our $UA_TRIGGER;	# Trigger only on procview in UserAgent

# Be aware that logging goes to either main or vhost log depending on whether
# in Init handler (main) or cleanup handler (vhost)
sub debug {
    Apache2::ServerRec->log->debug($_[0]);
}

sub cleanup_procview {
    debug("Cleaning up for server $$!!");
    if (defined $SHM) {
        $SHM->detach;
        $SHM->remove;
        $SHM = undef;
    }
}

sub setup_procview {
    my $s = shift;
    debug("Enabling for server $$!!");
    $PATH = $s->dir_config('Apache2-Procview-TracePath');
    $PATH = '/tmp' unless defined $PATH;
    $SCORE_INTERVAL = $s->dir_config('Apache2-Procview-ScoreInterval');
    $SCORE_INTERVAL = 0 unless defined $SCORE_INTERVAL;
    $MAX_TRACE = $s->dir_config('Apache2-Procview-MaxTrace');
    $MAX_TRACE = 0 unless defined $MAX_TRACE;
    $UA_TRIGGER = $s->dir_config('Apache2-Procview-Useragent');
    $UA_TRIGGER = 0 unless defined $UA_TRIGGER;
    $s->log->notice(sprintf(__PACKAGE__." Variables PATH: %s, SCORE_INTERVAL: %d, MAX_TRACE: %d, UA_TRIGGER: %d", $PATH, $SCORE_INTERVAL, $MAX_TRACE, $UA_TRIGGER));
    Apache2::ServerUtil::server_shutdown_cleanup_register(\&cleanup_procview);
    $SHM = IPC::SharedMem->new($$, 12, SHM_PERMS | IPC_CREAT);
    return Apache2::Const::SERVER_ERROR unless defined $SHM;
    $SHM->attach;
    Linux::Futex::init($SHM->addr);
    setvars(0,0);
    $s->log->notice(sprintf(__PACKAGE__." Using shmid %d", $SHM->id));
    return Apache2::Const::OK;
}

sub post_config {
    my ($conf_pool, $log_pool, $temp_pool, $s) = @_;
    return (Apache2::ServerUtil::restart_count() > 1) ? setup_procview($s): Apache2::Const::OK;
}

sub child_init {
    my ($child_pool, $s) = @_;
    my $conf_pool = $s->process->pconf;
    $SHM->attach;
    debug(sprintf("Child $$ attached shmid %d",$SHM->id));
    return Apache2::Const::OK;
}

sub getvars {
    my ($count, $tstamp) = unpack("LL", $SHM->read(4, 8));
    debug("getvars = ($count, $tstamp)");
    return ($count, $tstamp);
}

sub setvars {
    my ($count, $tstamp) = @_;
    $SHM->write(pack("LL", $count, $tstamp), 4, 8);
    debug("setvars($count, $tstamp)");
}

sub lock {
    debug("Locking in $$");
    Linux::Futex::lock($SHM->addr);
    debug("Locked in $$");
}

sub unlock {
    debug("Unlocking in $$");
    Linux::Futex::unlock($SHM->addr);
    debug("Unlocked in $$");
}

sub dumpScoreboard {

    my ($r) = @_;

    my $image = Apache::Scoreboard->image($r->pool);

    my %scoreboard_key = (
        '_' => 'Waiting for Connection',
        'S' =>  'Starting up',
        'R' => 'Reading Request',
        'W' => 'Sending Reply',
        'K' => 'Keepalive (read)',
        'D' => 'DNS Lookup',
        'C' =>  'Closing connection',
        'L' => 'Logging',
        'G' => 'Gracefully finishing',
        'I' => 'Idle cleanup of worker',
        '.' => 'Open slot with no current process'
    );

    my %worker_stats = map { $_ => 0 } keys %scoreboard_key;
    my @worker_scores;

    my $waiting_for_connections_count = 0;
    my $writing_response = 0;

    for (my $parent_score = $image->parent_score;
            $parent_score;
            $parent_score = $parent_score->next
        ) {

        my $pid = $parent_score->pid;

        my $worker_score = $parent_score->worker_score;

	$r->server->log->alert (__PACKAGE__." Scoreboard pid:$pid" .
            " ".$worker_score->status .
            " ".$worker_score->request .
            " ".$worker_score->client .
            " ".$worker_score->req_time."mS" .
            " conn: ".$worker_score->conn_count .
            "/".$worker_score->conn_bytes .
            " worker: ".$worker_score->access_count .
            "/".$worker_score->bytes_served .
            "/".$worker_score->times."%".
            " <".$worker_score->vhost.">");

        $worker_stats{$worker_score->status}++;
    }
    local $Data::Dumper::Terse = 1;
    local $Data::Dumper::Indent = 0;
    my $stats = Dumper(%worker_stats);
    $r->server->log->alert (__PACKAGE__." Scoreboard statuses: $stats");

}

sub writeout : method {
    my ($class, $r) = @_;

    my $notes = $r->pnotes($class) || {};

    kill INT => $notes->{strace_pid};
    waitpid($notes->{strace_pid},0);

    my $req = $r->the_request;
    my $pid = $notes->{pid};
    my $sec = $notes->{sec};
    my $usec = $notes->{usec};

    my $reportname = "$PATH/procview.$pid.$sec.$usec.trace";
    my $report;
    open($report, "> $reportname");

    my @time = localtime $sec;
    printf $report "TIME %04d-%02d-%02d at %02d:%02d:%02d.%06d on %s\n",
	    $time[5]+1900,$time[4]+1,$time[3],$time[2],$time[1],$time[0],$usec, hostname;
    print $report "REQUEST ", $req, "\n";
    my $nhandles = 0;
    foreach (split(/\n/,$notes->{lsof})) {
	my @fields = split(/\s+/);
	if ($fields[3] =~ /(\d+\w|cwd)/) {
	    print $report "LSOF ", $_, "\n";
            $nhandles++;
	}
    }
    if ($nhandles == 0) {
        $r->log->emerg(__PACKAGE__." lsof detected no handles - is it setuid?");
    }
    open (FH, "< ".$notes->{strace_file}) or die "Cannot open strace file";
    my $ntrace = 0;
    while (<FH>) {
	print $report $_;
        $ntrace++;
    }
    if ($ntrace == 0) {
        $r->log->emerg(__PACKAGE__." strace produced no output - is it setuid?");
    }
    close (FH);
    unlink ($notes->{strace_file});
    close ($report);


    $r->log->notice(__PACKAGE__." IO Trace at $reportname");

    debug ("Cleanup $req");
    my $lock_delay = $r->dir_config('Apache2-Procview-LockDelay');
    if (defined($lock_delay) && $req =~ /sleep/) {
	lock();
        debug("$$ sleeping holding lock");
        sleep($lock_delay);
        debug("$$ waking holding lock");
	unlock();
    }
    return Apache2::Const::OK;
}
sub handler : method {
    my ($class, $r) = @_;

    if (-e "$PATH/DisableProcviewTrace") {
        debug("Trace disabled by existence of $PATH/DisableProcviewTrace");
	return Apache2::Const::OK;
    }
    my $trace_request = 0;
    if ( $UA_TRIGGER > 0 ) {
        my $ua = $r->headers_in->get( 'User-Agent' ) || 'notfound';
        $trace_request = 1 if $ua =~ m/procview$/i;
    }
    my $servers_left = 1;

    if ($SCORE_INTERVAL > 0) {
        my $image = Apache::Scoreboard->image($r->pool);
        $servers_left = $image->server_limit - scalar(@{$image->pids});
        debug("Using ".scalar(@{$image->pids})." servers, limit is ".$image->server_limit);

        if ($servers_left == 0) {
	    my $dump = 0;
            lock();
            my ($count, $fulltime) = getvars();
            my $now = time();
            if  ($now - $fulltime > $SCORE_INTERVAL) {
                debug("All Servers used and score interval reached at $now");
		setvars(0, $now);
                $dump = 1;
            }
            unlock();
            dumpScoreboard ($r) if $dump > 0;
        }
    }
    my $pid = $$;
    lock();
    my ($count, $fulltime) = getvars();
    if ($trace_request == 1 || ($servers_left == 0 && $count < $MAX_TRACE)) {
	$r->push_handlers('CleanupHandler' => "${class}->writeout" );
	my $notes = $r->pnotes($class) || {};
	my $strace_file = "$PATH/$pid.strace";
	my @strace_args = ("-Tfq", "-e", "desc,network,file,process", "-p", $pid, "-o", $strace_file);

	$notes->{strace_file} = $strace_file;
	$notes->{strace_pid} = fork();
	if ($notes->{strace_pid} == 0) {
	    exec(STRACE, @strace_args);
	}

	my $lsof_cmd = LSOF." -p $pid 2>&1";
	$notes->{lsof} = `$lsof_cmd`;

	$notes->{pid} = $pid;
	my ($sec,$usec) = gettimeofday();
	$notes->{sec} = $sec;
	$notes->{usec} = $usec;
	$r->pnotes($class, $notes);
	$count++;
	debug("Tracing request ".$r->the_request);
    }
    setvars($count, $fulltime);
    unlock();
    return Apache2::Const::OK;
}

1;

# vim: noet sts=4 sw=4 ts=8
