use strict;
use warnings FATAL => 'all';

use Apache::Test;
use Apache::TestRequest;
use Apache::TestUtil;

plan tests => 5;

ok GET_OK '/index.html';
my @vars = `grep 'Apache2::Procview Variables' t/logs/error_log`;
ok t_cmp(scalar(@vars), 1, "Only one variable log");
my $vars = pop @vars;
ok t_cmp($vars, qr/SCORE_INTERVAL: 15\b/, "Score Interval Set");
ok t_cmp($vars, qr/MAX_TRACE: 2\b/, "Max Trace set");
ok t_cmp($vars, qr/UA_TRIGGER: 1\b/, "Useragent trigger set");
