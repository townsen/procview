use strict;
use warnings FATAL => 'all';

use Apache::Test;
use Apache::TestRequest;
use Apache::TestUtil;

plan tests => 4, need_lwp;

Apache::TestRequest::user_agent(reset => 1);

GET_OK '/index.html';
my @a = `grep 'Apache2::Procview IO Trace at' t/logs/error_log`;
ok t_cmp(scalar(@a), 0, "No Trace file should exist");

Apache::TestRequest::user_agent(reset => 1, agent => "useragent/test procview");

GET_OK '/index.html';
my @b = `grep 'Apache2::Procview IO Trace at' t/logs/error_log`;
ok t_cmp(scalar(@b), 1, "Trace file log entry exists");
my $b = pop @b;
my $bf = $1 if $b =~ /IO Trace at (\S+)/;
ok t_cmp(-f $bf, 1, "Trace file exists");

t_write_file('t/htdocs/DisableProcviewTrace',"");
GET_OK '/index.html';
my @c = `grep 'Apache2::Procview IO Trace at' t/logs/error_log`;
ok t_cmp(scalar(@c), 1, "Trace file creation suppressed");
