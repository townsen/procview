%define module  Apache2-Procview

Name:           perl-%{module}
Version:        0.42
Release:        1%{?dist}
Summary:        I/O Profile Apache requests using mod_perl
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            https://github.com/townsen/procview
Source0:        %{module}-%{version}.tar.gz
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl(ExtUtils::MakeMaker)
BuildRequires:  perl(Exporter)
BuildRequires:  perl(Test)
BuildRequires:  perl(Test::More)
BuildRequires:  perl(Linux::Futex)
BuildRequires:  perl(Test::Pod)
BuildRequires:  perl(Apache::Scoreboard)
BuildRequires:  perl(IPC::SharedMem)
BuildRequires:  perl(ModPerl::MM)
BuildRequires:  perl(Sys::Hostname)
BuildRequires:  perl(Time::HiRes)
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))
Requires:       mod_perl >= 2.0
Requires:       lsof >= 4.78
Requires:       strace >= 4.5.18

%{?perl_default_filter}

%description
Apache2::Procview allows manual and automatic recording of the
I/O characteristics of web requests. It writes tracefile
that are later post-processed by the procview gem.

%prep
%setup -n %{module}-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
%{__make} %{?_smp_mflags}

%install
%{__rm} -rf $RPM_BUILD_ROOT

%{__make} pure_install PERL_INSTALL_ROOT=$RPM_BUILD_ROOT

find $RPM_BUILD_ROOT -type f -name .packlist -exec rm -f {} \;
find $RPM_BUILD_ROOT -depth -type d -exec rmdir {} 2>/dev/null \;

%{_fixperms} $RPM_BUILD_ROOT/*

%check
make test

%post

%{__chmod} u+s /usr/bin/strace /usr/sbin/lsof

%clean
%{__rm} -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc /usr/share/man/man3/Apache2::Procview.3pm.gz
%{perl_vendorlib}/*

%changelog
* Fri Feb 21 2014 Nick Townsend <nick.townsend@mac.com> - 0.30-1
- Incorporated automatic triggering when scoreboard full
- Added limits
- First RPM packaged version for RHEL 6
