#!/usr/bin/perl

use strict;
use warnings;

use ModPerl::MM ();
use Apache::TestMM qw(test clean);
use Apache::TestRunPerl ();

Apache::TestMM::filter_args();

Apache::TestRunPerl->generate_script();

ModPerl::MM::WriteMakefile(
    META_MERGE => {
        resources => {
            repository => 'https://github.com/townsen/procview',
        },
    },
    NAME      => 'Apache2::Procview',
    VERSION_FROM => 'lib/Apache2/Procview.pm',
    SIGN => 0,
    PREREQ_PM => {
        'mod_perl2'   => '2',
        'Test::More' => '0.47',
        'Linux::Futex' => '0.6',
    },
);
